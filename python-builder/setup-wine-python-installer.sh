#!/bin/bash
pwd # Should be ${CI_PROJECT_DIR}
cat /etc/os-release # For debug
export DEBIAN_FRONTEND=noninteractive # For auto install without asking user

# Install Intel 80386 architecture for Wine to work
dpkg --add-architecture i386

# Update ubuntu image
apt-get update -qy
# Install some dependencies and usefull libs
apt-get install --no-install-recommends -qfy apt-transport-https software-properties-common wget xvfb xdotool x11-utils xterm gpg-agent rename git
    
# Install Wine
wget -nv https://dl.winehq.org/wine-builds/winehq.key
apt-key add winehq.key
add-apt-repository 'https://dl.winehq.org/wine-builds/ubuntu/'
apt-get update -qy
apt-get install --no-install-recommends -qfy winehq-stable winbind cabextract
apt-get clean
wget -nv https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
chmod +x winetricks
mv winetricks /usr/local/bin

# Set environment variable for Wine
# Architecture 64bits
export WINEARCH=win64 #winecfg
# Don't export class channel fixme and error
export WINEDEBUG=fixme-all,err-all
export WINEPREFIX=/wine

# xvfb settings
export DISPLAY=:0
echo 'Xvfb $DISPLAY -screen 0 1024x768x24 &' >> /root/.bashrc
    
# PYPI repository location
export PYPI_URL=https://pypi.python.org/
# PYPI index location
export PYPI_INDEX_URL=https://pypi.python.org/simple

# Install python in wine, using the msi packages to install, extracting the files directly, since installing isn't running correctly.
# The next command set up wine as windows 7 (win7) or XP (winxp), both works
winetricks win7
for msifile in `echo core dev exe lib path pip tcltk tools`; do wget -nv "https://www.python.org/ftp/python/3.11.2/amd64/${msifile}.msi"; wine msiexec /i "${msifile}.msi" /qb TARGETDIR=C:/Python311; rm ${msifile}.msi; done
cd /wine/drive_c/Python311
echo 'wine '\''C:\Python311\python.exe'\'' "$@"' > /usr/bin/python
echo 'wine '\''C:\Python311\Scripts\easy_install.exe'\'' "$@"' > /usr/bin/easy_install
echo 'wine '\''C:\Python311\Scripts\pip.exe'\'' "$@"' > /usr/bin/pip
echo 'wine '\''C:\Python311\Scripts\pyinstaller.exe'\'' "$@"' > /usr/bin/pyinstaller
echo 'wine '\''C:\Python311\Scripts\pyupdater.exe'\'' "$@"' > /usr/bin/pyupdater
echo 'wine '\''C:\Python311\Scripts\pyi-makespec.exe'\'' "$@"' > /usr/bin/pyi-makespec
echo 'assoc .py=PythonScript' | wine cmd
echo 'ftype PythonScript=c:\Python311\python.exe "%1" %*' | wine cmd
while pgrep wineserver >/dev/null; do echo "Waiting for wineserver"; sleep 1; done
chmod +x /usr/bin/python /usr/bin/easy_install /usr/bin/pip /usr/bin/pyinstaller /usr/bin/pyupdater /usr/bin/pyi-makespec
(pip install -U pip || true)
rm -rf /tmp/.wine-*
export W_DRIVE_C=/wine/drive_c
export W_WINDIR_UNIX="$W_DRIVE_C/windows"
export W_SYSTEM64_DLLS="$W_WINDIR_UNIX/system32"
export W_TMP="$W_DRIVE_C/windows/temp/_$0"

# Install Microsoft Visual C++ Redistributable for Visual Studio 2017 dll files
# Solve error of Microsoft Visual C++ 14.0 or greater required
rm -f "$W_TMP"/*
wget -P "$W_TMP" https://aka.ms/vs/16/release/vc_redist.x64.exe
# https://download.visualstudio.microsoft.com/download/pr/36e45907-8554-4390-ba70-9f6306924167/97CC5066EB3C7246CF89B735AE0F5A5304A7EE33DC087D65D9DFF3A1A73FE803/VC_redist.x64.exe
ls "$W_TMP"/
cabextract -q --directory="$W_TMP" "$W_TMP"/vc_redist.x64.exe
cabextract -q --directory="$W_TMP" "$W_TMP/a11"
cabextract -q --directory="$W_TMP" "$W_TMP/a12"
cabextract -q --directory="$W_TMP" "$W_TMP/a13"
cd "$W_TMP"
rename 's/_/\-/g' *.dll
cp "$W_TMP"/*.dll "$W_SYSTEM64_DLLS"

# Install pyinstaller
/usr/bin/python -m pip install --upgrade pip
/usr/bin/pip install --upgrade pyinstaller

#kill wineserver before building docker image
wineserver -k
