#!/bin/bash

export DEBIAN_FRONTEND=noninteractive # For auto install without asking user
# Architecture 64bits
export WINEARCH=win64 #winecfg
# Don't export class channel fixme and error
export WINEDEBUG=fixme-all,err-all
export WINEPREFIX=/wine

# PYPI repository location
export PYPI_URL=https://pypi.python.org/
# PYPI index location
export PYPI_INDEX_URL=https://pypi.python.org/simple

export W_DRIVE_C=/wine/drive_c
export W_WINDIR_UNIX="$W_DRIVE_C/windows"
export W_SYSTEM64_DLLS="$W_WINDIR_UNIX/system32"
export W_TMP="$W_DRIVE_C/windows/temp/_$0"


# xvfb settings (need x server to run wine)
export DISPLAY=:0
echo 'Xvfb $DISPLAY -screen 0 1024x768x24 &' >> /root/.bashrc
    
#Generate version from git
cd ${CI_PROJECT_DIR}
export GITVER=$(git describe --always --dirty --tags)
echo "Obtained git version: $GITVER"

# Copy repository files in wine src
ls -al /wine/drive_c
mkdir /wine/drive_c/src
cp -ar ${CI_PROJECT_DIR}/. /wine/drive_c/src/
ls -al /wine/drive_c/src

