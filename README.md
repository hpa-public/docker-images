
# BE/GM/HPA Docker Images

Repository containing docker images used by the BE/GM/HPA section at CERN

## python-builder

A docker image that allows building windows .exe from python, inside a linux host.
Usage in CI:
```
  - export TARGET=targetName
  - export DEST=destName.exe
  - source /opt/src/scripts/setup_ci_environment.sh
# Replace all instances of @VERSION@ by found git version
  - find /wine/drive_c/src/ -type f -name '*.py' -exec sed -i "s/@VERSION@/$GITVER/g" '{}'
# Install requirements into wine version of pip
  - cd /wine/drive_c/src/
  - /usr/bin/pip install -r requirements.txt
# Create executable file with pyinstaller
#--noconsole --noupx --icon=images/Rabot.ico --add-data="images/*;images/." --add-data="*.ui;."
  - pyi-makespec --onefile --noupx $TARGET.py
  - pyinstaller --clean $TARGET.spec
# Move .exe
  - ls -al dist/
  - cp -rv /wine/drive_c/src/dist/$DEST ${CI_PROJECT_DIR}
```
